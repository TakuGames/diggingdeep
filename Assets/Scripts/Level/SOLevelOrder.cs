﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLevelOrder", menuName = "LevelOrder")]
public class SOLevelOrder : ScriptableObject
{
    [SerializeField] private SOLevelConfig[] _levelConfigs;

    public SOLevelConfig GetLevelConfig(int number)
    {
        return _levelConfigs[number];
    }

    public int levelCount => _levelConfigs.Length;
}
