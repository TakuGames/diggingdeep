﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLevel : MonoBehaviour
{
    public static Action OnEndLevel;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        OnEndLevel.Invoke();
    }
}
