﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "NewLevelConfig", menuName = "LevelConfig")]
public class SOLevelConfig : ScriptableObject
{
    [SerializeField] private Tilemap _startChunk;
    [SerializeField] private Tilemap _endChunk;
    [SerializeField] private Tilemap[] _chunkToChoose;

    public Tilemap randomBasisChunk
    {
        get
        {
            var chunk = _chunkToChoose[Random.Range(0, _chunkToChoose.Length)];
            chunk.CompressBounds();
            return chunk;
        }
    }

    public Tilemap startChunk
    {
        get
        {
            _startChunk.CompressBounds();
            return _startChunk;
        }
    }

    public Tilemap endChunk
    {
        get
        {
            _endChunk.CompressBounds();
            return _endChunk;
        }
    }
}
