﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelPlacer : MonoBehaviour
{
    [SerializeField] private SOLevelOrder _levelOrder;
    private static int _levelCount;
    
    private SOLevelConfig _levelConfig;
    private static Vector2 _startPosition;
    private static float _top;
    private static float _bottom;
    private static float _width;
    
    private static Tilemap _chunkBasis;
    private static Tilemap _chunkStart;
    private static Tilemap _chunkEnd;

    public static Vector2 startPosition => _startPosition;
    public static float top => _top;
    public static float bottom => _bottom;
    public static float width => _width;
    public static int levelCount => _levelCount;

    private void Awake()
    {
        int number = PlayerPrefs.GetInt("levelNumber");
        _levelConfig = _levelOrder.GetLevelConfig(number);
        _levelCount = _levelOrder.levelCount;
        
        foreach (Transform child in gameObject.transform)
        {
            Destroy(child.gameObject);
        }

        float posY;
        float posX;
        
        _chunkBasis = _levelConfig.randomBasisChunk;
        _chunkStart = _levelConfig.startChunk;
        _chunkEnd = _levelConfig.endChunk;

        posY = 0;
        posX = -_chunkBasis.cellBounds.center.x;
        
        Instantiate(_chunkBasis.gameObject, new Vector2(posX, posY), Quaternion.identity, gameObject.transform);

        posY = _chunkBasis.cellBounds.center.y + _chunkBasis.size.y * 0.5f + _chunkStart.size.y * 0.5f - _chunkStart.cellBounds.center.y;
        Instantiate(_chunkStart.gameObject, new Vector2(posX, posY), Quaternion.identity, gameObject.transform);
        
        posY = _chunkBasis.cellBounds.center.y - _chunkBasis.size.y * 0.5f - _chunkEnd.size.y * 0.5f - _chunkEnd.cellBounds.center.y;
        Instantiate(_chunkEnd.gameObject, new Vector2(posX, posY), Quaternion.identity, gameObject.transform);
        
        
        AssignTopPosition();
        AssignBottomPosition();
        AssignWidth();
            
        AssignStartPosition();
    }

    private void AssignStartPosition()
    {
        float posX = 0;
        //float posY = _top + 1f;
        float posY = _top;
        
        _startPosition = new Vector2(posX, posY);
    }

    private void AssignTopPosition()
    {
        _top = _chunkBasis.cellBounds.center.y + _chunkBasis.size.y * 0.5f + _chunkStart.size.y;
    }
    
    private void AssignBottomPosition()
    {
        _bottom = _chunkBasis.cellBounds.center.y - _chunkBasis.size.y * 0.5f - _chunkEnd.size.y;
    }

    private void AssignWidth()
    {
        _width = _chunkBasis.size.x;
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("levelNumber", 0);
    }
}
