﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GoldUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;

    private string _startText;

    private void Start()
    {
        if (_text)
            _startText = _text.text;
    }
    void Update()
    {
        if (_text)
            _text.SetText(_startText + ScoreController.GoldValue.ToString());
    }
}
