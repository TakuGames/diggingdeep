﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject _finishGameUI;
    [SerializeField] private GameObject _gameOverUI;

    private void Awake()
    {
        _finishGameUI.SetActive(false);
        _gameOverUI.SetActive(false);
    }

    private void OnEnable()
    {
        EndLevel.OnEndLevel += NextLevel;
        PlayerHealth.OnDead += GameOver;
    }
    private void OnDisable()
    {
        EndLevel.OnEndLevel -= NextLevel;
        PlayerHealth.OnDead -= GameOver;
    }

    void GameOver()
    {
        _gameOverUI.SetActive(true);
        Time.timeScale = 0;
    }

    private void NextLevel()
    {
        int levelNumber = PlayerPrefs.GetInt("levelNumber");

        if (levelNumber >= LevelPlacer.levelCount - 1)
        {
            FinishGame();
        }
        else
        {
            levelNumber++;
            PlayerPrefs.SetInt("levelNumber", levelNumber);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    private void FinishGame()
    {
        _finishGameUI.SetActive(true);
        Time.timeScale = 0;
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("levelNumber", 0);
    }

    public void Restart()
    {
        Time.timeScale = 1;
        ResetController();
    }

    public void ResetController()
    {
        ScoreController.GoldValue = 0;      
        JoysticDirections.PlayerDirection = EDirection.None;
        
        PlayerPrefs.SetInt("levelNumber", 0);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
