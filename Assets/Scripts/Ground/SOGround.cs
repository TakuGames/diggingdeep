﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Ground Config", menuName = "Configs/GroundConfig")]
public class SOGround : ScriptableObject
{
    public float StartHealth;
    
    public Sprite[] DamagedSprites;
}
