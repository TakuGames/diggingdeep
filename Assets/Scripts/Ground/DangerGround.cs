﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DangerGround : MonoBehaviour
{
    [SerializeField] private SODanger _soDanger;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag != "Player") return;

        var damagedObject = collision.collider.GetComponent<IDamageble>();
        if (damagedObject == null) return;


        if (_soDanger.ImmediateDeath) 
            damagedObject.Damage(_soDanger.DamageAmount);
        else
            damagedObject.Damage(float.MaxValue);

    }
}
