﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new Danger Config", menuName = "Configs/DangerConfig")]
public class SODanger : ScriptableObject
{
    public float DamageAmount;
    public bool ImmediateDeath;
}
