﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerRebounce : MonoBehaviour
{
    private PlayerController _player;
    private Vector2 _dir = Vector2.zero;

    private float _damagedJump = 0;

    private void Start()
    {
        _player = GetComponent<PlayerController>();
    }

    public void Rebounce(EDirection direction, GameObject obj, float distance)
    {
        if (distance <= _player.Config.ContactDistance)
        {
            //_damagedJump = (JoysticDirections.PlayerDirection == EDirection.Down) ? _player.Config.ForceJumpDamage : _player.Config.ForceJump;

            Vector2 forceDir = new Vector2(0, _player.Config.ForceJump);
            _player.Rb.velocity = Vector2.zero;

            switch (direction)
            {
                case EDirection.Right:
                    //forceDir.x = -_player.Config.ForceRebound;
                    break;
                case EDirection.Left:
                    //forceDir.x = _player.Config.ForceRebound;
                    break;
                default:
                    break;
            }

            forceDir.y = _damagedJump;
            _player.Rb.AddForce(forceDir, _player.Config.RebounceMode);
            _damagedJump = _player.Config.ForceJump;
        }
    }
}
