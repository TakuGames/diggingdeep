﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour, IDamageble
{
    public static Action OnDead;

    private float _health;
    // Start is called before the first frame update
    void Start()
    {
        _health = GetComponent<PlayerController>().Config.StartHealth;
    }

    public void Damage(float damageCount)
    {
        _health -= damageCount;
        if (_health <= 0)
        {
            OnDead.Invoke();
        }           
    }
}
