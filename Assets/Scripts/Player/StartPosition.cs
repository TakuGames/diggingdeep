﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPosition : MonoBehaviour
{
    [SerializeField] private float _startDelay = 1;

    private float _currentTime = 0;
    private PlayerController _player = null;

    private void Start()
    {
        _player = GetComponent<PlayerController>();
        if (_player)
            _player.enabled = false;

        gameObject.transform.position = LevelPlacer.startPosition;
    }

    private void Update()
    {
        if (_currentTime <= _startDelay)
        {
            _currentTime += Time.deltaTime;
        }
        else
        {
            if(!_player.enabled)
                _player.enabled = true;
        }    
    }
}
