﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private SOPlayer PlayerConfig;

    public Collider2D Collider => _collider;
    public Rigidbody2D Rb => _rigidbody;
    public SOPlayer Config => PlayerConfig;
    public Animator Animator => _animator;

    private Animator _animator;
    private Collider2D _collider;
    private Rigidbody2D _rigidbody;

    private IRayable _rayable;
    private IMovable _movable;
    private IDetecable _detecable;

    void Start()
    {
        _animator = GetComponent<Animator>();

        _collider = GetComponent<Collider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();

        _rayable = GetComponent<IRayable>();
        _movable = GetComponent<IMovable>();
        _detecable = GetComponent<IDetecable>();
    }

    void Update()
    {
        if (_detecable != null)
            _detecable.DetectionDirection(this);

        if (_rayable != null)
            _rayable.Looking(this);

        if (_movable != null)
        {
            _movable.Move(this);
            if (JoysticDirections.PlayerDirection == EDirection.Up)
                _movable.Rebound(this);
        }
    }
}
