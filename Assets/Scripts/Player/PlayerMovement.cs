﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour, IMovable
{
    public void Move(PlayerController playerController)
    {
        if (!playerController)
            return;

        int direct;

        switch (JoysticDirections.PlayerDirection)
        {
            case EDirection.Left:
                direct = -1;
                break;
            case EDirection.Right:
                direct = 1;
                break;
            default:
                direct = 0;
                break;
        }
        playerController.Rb.velocity = new Vector2(direct * playerController.Config.HorizontalSpeed, playerController.Rb.velocity.y);
        //playerController.transform.Translate(Vector2.right * direct * playerController.Config.HorizontalSpeed * Time.deltaTime);
    }
    public void Rebound(PlayerController playerController)
    {
        var _direct = Vector2.down;
        var _rayPoint = new Vector2(transform.position.x, playerController.Collider.bounds.min.y);

        RaycastHit2D hit = Physics2D.Raycast(_rayPoint, _direct, 0.1f);

        if (!hit.collider) return;

        Vector2 forceDir = new Vector2(0, playerController.Config.ForceJump);
        playerController.Rb.velocity = Vector2.zero;
        playerController.Rb.AddForce(forceDir, playerController.Config.RebounceMode);
    }
}
