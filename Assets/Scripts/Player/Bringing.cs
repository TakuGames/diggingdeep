﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bringing : MonoBehaviour
{
    [SerializeField] private float _distanceToTile;

    private EDirection _lastHorizontalDirection;
    private Collider2D _collider;
    private PlayerController _playerController;

    private float _distance;
    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider2D>();
        _playerController = GetComponent<PlayerController>();
                
        _distance = _collider.bounds.size.y * 0.5f + _distanceToTile;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (JoysticDirections.PlayerDirection == EDirection.Left || JoysticDirections.PlayerDirection == EDirection.Right)
            _lastHorizontalDirection = JoysticDirections.PlayerDirection;

        if (JoysticDirections.PlayerDirection != EDirection.None)
            return;

        float x;
        float y;               

        y = _collider.bounds.center.y;
        x = _collider.bounds.min.x;

        RaycastHit2D hitLeft = Physics2D.Raycast(new Vector2(x, y), Vector2.down, _distance);
        Debug.DrawLine(new Vector2(x, y), new Vector2(x, y) + Vector2.down * _distance);

        x = _collider.bounds.max.x;

        RaycastHit2D hitRight = Physics2D.Raycast(new Vector2(x, y), Vector2.down, _distance);
        Debug.DrawLine(new Vector2(x, y), new Vector2(x, y) + Vector2.down * _distance);

        if (!hitLeft.collider && !hitRight.collider)
            return;

        int direct;

        if (hitLeft.collider == hitRight.collider)
            return;

       direct  = _lastHorizontalDirection == EDirection.Right ? 1 : _lastHorizontalDirection == EDirection.Left ? -1 : 0;
      

        _playerController.Rb.velocity = new Vector2(direct * _playerController.Config.HorizontalSpeed, _playerController.Rb.velocity.y);
        //_playerController.transform.Translate(Vector2.right * direct * _playerController.Config.HorizontalSpeed * Time.deltaTime);
    }
}
