﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = " new Player Config", menuName = "Configs/PlayerConfig")]
public class SOPlayer : ScriptableObject
{
    [Header("[State Settings]")]
    [Space(10)]
    public float DamageAmount;
    public float StartHealth;

    [Header("[Movement Settings]")]
    [Space(10)]
    public float ForceJump;
    //public float ForceJumpDamage;
    //public float ForceRebound;
    
    public float HorizontalSpeed;
    public ForceMode2D RebounceMode;
    public ForceMode2D MoveMode;
    public float ContactDistance;
    public float LimitMovement;
}
