﻿using SimpleInputNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EDirection
{
    //TODO remove None
    None, Right, Left, Down, Up
}

public class JoysticDirections : MonoBehaviour, IDetecable
{

    public delegate void ChangeDirectionDelegate( EDirection oldDirection, EDirection newDirection);

    public static event ChangeDirectionDelegate ChangeDirectionEvent;
    public static EDirection PlayerDirection = EDirection.Down;

    private void Start()
    {
        PlayerDirection = EDirection.None;
        ChangeDirectionEvent?.Invoke( EDirection.None, PlayerDirection); 
        Debug.Log("ok");
    }


    public void DetectionDirection(PlayerController controller)
    {
        var oldDirection = PlayerDirection;

        if (Joystick.yAxis.value < 0)
        {
            if (Mathf.Abs(Joystick.yAxis.value) > Mathf.Abs(Joystick.xAxis.value))
                PlayerDirection = EDirection.Down;
            else
                PlayerDirection = (Joystick.xAxis.value >= 0) ? EDirection.Right : EDirection.Left;
        }
        else if (Joystick.yAxis.value > 0)
        {
            if (Mathf.Abs(Joystick.yAxis.value) > Mathf.Abs(Joystick.xAxis.value))
                PlayerDirection = EDirection.Up;
            else
                PlayerDirection = (Joystick.xAxis.value >= 0) ? EDirection.Right : EDirection.Left;
        }
        else
        {
            //if()
            PlayerDirection = EDirection.None;
        }

        if (oldDirection != PlayerDirection)
            ChangeDirectionEvent?.Invoke( oldDirection, PlayerDirection);    
    } 
}
