﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControllerOld : MonoBehaviour
{
    [SerializeField] private SOPlayer PlayerConfig;   

    public Collider2D Collider => _collider;
    public Rigidbody2D Rb => _rigidbody;
    public SOPlayer Config => PlayerConfig;
    public Animator Animator => _animator;

    private Animator _animator;
    private Collider2D _collider;
    private Rigidbody2D _rigidbody;

    //private IReboundable _reboundable;
    private IMovable _movable;
    private IDetecable _detecable;

    void Start()
    {
        _animator = GetComponent<Animator>();

        _collider = GetComponent<Collider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();

       // _reboundable = GetComponent<IReboundable>();
        _movable = GetComponent<IMovable>();
        _detecable = GetComponent<IDetecable>();
    }

    void FixedUpdate()
    {
//        if (_detecable != null)
//            _detecable.DetectionDirection(this);
//
//        if (_reboundable != null)
//            _reboundable.Rebounce(this);
//
//        if (_movable != null)
//            _movable.Move(this);
    }
}
