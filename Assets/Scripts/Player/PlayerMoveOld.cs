﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleInputNamespace;

public class PlayerMoveOld : MonoBehaviour//, IMovable
{
    private bool _stop = false;

    private float _contactDistance = 0.1f;
    private float _limitX = 0.2f;

    Vector2 direction = Vector2.zero;
    Vector2 currentVelocity = Vector2.zero;

    Coroutine stopMoving = null;
    WaitForSeconds sec = new WaitForSeconds(0.2f);


    IEnumerator StopMove()
    {
        _stop = true;

        yield return sec;

        _stop = false;

        stopMoving = null;
    }

    public void Move(PlayerController playerController)
    {
//        if (!playerController)
//            return;
//
//        _contactDistance = playerController.Config.ContactDistance;
//        _limitX = playerController.Config.LimitMovement;
//        
//        if (_stop)
//            return;
//
//        currentVelocity = playerController.Rb.velocity;
//        currentVelocity.x = 0;
//        playerController.Rb.velocity = currentVelocity;
//
//        if (Mathf.Abs(Joystick.InputX.value) <= _limitX)
//            return;
//
//        direction = new Vector2(Joystick.NormX, 0);
//        
//        playerController.Rb.AddForce(direction * playerController.Config.HorizontalSpeed, playerController.Config.MoveMode);
    }

    public void StopInput(EDirection direction, GameObject gameObject, float distance)
    {
        if (distance <= _contactDistance)
        {
            if (direction == EDirection.Right || direction == EDirection.Left)
                StopMoving();
        }
    }

    void StopMoving()
    {
        if (stopMoving == null)
        {
            stopMoving = StartCoroutine(StopMove());
        }
        else
        {
            StopCoroutine(StopMove());
            stopMoving = null;
        }
    }
}