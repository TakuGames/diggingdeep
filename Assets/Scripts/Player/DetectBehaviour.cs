﻿using SimpleInputNamespace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    None, Right, Left, Down, Up
}

public class DetectBehaviour : MonoBehaviour, IDetecable
{
    private static bool _onlyDown = false;

    public delegate void ChangeDirectionDelegate(Animator animator, Direction oldDirection, Direction newDirection);

    public static event ChangeDirectionDelegate ChangeDirectionEvent;
    public static Direction PlayerDirection = Direction.Down;


    public static bool OnlyDown { get => _onlyDown; set => _onlyDown = value; }


    public void DetectionDirection(PlayerController controller)
    {
        //Debug.Log(PlayerDirection);
        if (OnlyDown)
        {           
            if (Direction.Down != PlayerDirection)
                ChangeDirectionEvent?.Invoke(controller.Animator, PlayerDirection, Direction.Down);
            PlayerDirection = Direction.Down;
            return;
        }



        var oldDirection = PlayerDirection;

        if (Joystick.yAxis.value < 0)
        {
            if (Mathf.Abs(Joystick.yAxis.value) > Mathf.Abs(Joystick.xAxis.value))
                PlayerDirection = Direction.Down;
            else
                PlayerDirection = (Joystick.xAxis.value >= 0) ? Direction.Right : Direction.Left;
        }
        else if (Joystick.yAxis.value > 0)
        {
            if (Mathf.Abs(Joystick.yAxis.value) > Mathf.Abs(Joystick.xAxis.value))
                PlayerDirection = Direction.Up;
            else
                PlayerDirection = (Joystick.xAxis.value >= 0) ? Direction.Right : Direction.Left;
        }
        else
        {
            PlayerDirection = Direction.Down;
        }

        if (oldDirection != PlayerDirection)
            ChangeDirectionEvent?.Invoke(controller.Animator, oldDirection, PlayerDirection);

        
    }

    
    
}
