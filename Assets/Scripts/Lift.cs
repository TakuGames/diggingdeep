﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Lift : MonoBehaviour
{
    [SerializeField] private float _duration;
    [SerializeField] private AnimationCurve _curveUp;
    [SerializeField] private AnimationCurve _curveDown;

    private Vector3 _startPoint;
    private Vector3? _endPoint;
    private Vector3 _target;

    private Collider2D _collider;

    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider2D>();

        _startPoint = transform.position;
        _target = _startPoint;
        StartCoroutine(AfterStart());
    }

    private IEnumerator AfterStart()
    {
        yield return new WaitForEndOfFrame();

         
        //TODO: Raycast with mask
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up);

        if (!hit.collider)
            throw new System.ArgumentException("lift station collider  not found");

        if(hit.collider.GetComponent<LiftConnect>() == null)
            throw new System.ArgumentException("lift station ccomponent  not found");

        _endPoint = hit.collider.bounds.center + Vector3.down;
        Move();
    }

    private void Move()
    {
        var curve = _target == _startPoint ? _curveUp : _curveDown;
        _target = _target == _startPoint ? (Vector3)_endPoint : _startPoint;
        var seq = DOTween.Sequence();
        seq.Append(transform.DOMoveY(_target.y, _duration).SetEase(curve));
        seq.AppendCallback(Move);
        seq.Play();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.bounds.center.x < _collider.bounds.min.x &&
            collision.collider.bounds.center.x > _collider.bounds.max.x)
            return;

        if (collision.collider.bounds.center.y < _collider.bounds.center.y) return;

        if (collision.gameObject.GetComponent<Rigidbody2D>() == null) return;

        collision.gameObject.transform.SetParent(gameObject.transform);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        collision.gameObject.transform.SetParent(null);
    }
}
