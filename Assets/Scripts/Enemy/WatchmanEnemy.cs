﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WatchmanEnemy : AEnemy
{
    [SerializeField] private float _movementRange = 5f;
    [SerializeField] private float _distance = 0.1f;
    [SerializeField] private LayerMask _mask = 0;
    [SerializeField] private float _speed;


    private Vector2[] _waypoints;
    private int _curIndexPoint;

    private void Start()
    {
        _waypoints = new Vector2[]
        {
            transform.position,
            transform.position + Vector3.right * _movementRange
        };

        _curIndexPoint = _movementRange < 0 ? 0 : 1;
    }

    private void Update()
    {
        Move();
    }

    public void Move()
    {
        DetectDirection();
        transform.Translate(new Vector3(_speed, 0f, 0f) * Time.deltaTime);
    }

    private void DetectDirection()
    {
        if (Mathf.Abs(_waypoints[_curIndexPoint].x - transform.position.x) < 0.2f)
        {
            ChangeDirection();
            return;
        }

        var point = new Vector2(transform.rotation.y == 0 ? _collider.bounds.max.x : _collider.bounds.min.x, transform.position.y);
        RaycastHit2D hit = Physics2D.Raycast(point, transform.right, _distance, _mask);

        if (!hit)
            return;

        //Debug.DrawLine(point, hit.point *10, Color.red, 2f);

        if (hit.collider)
        {
            ChangeDirection();
            return;
        } 

        
    }

    private void ChangeDirection()
    {
        if (transform.rotation.y == 0)
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        else
            transform.rotation = Quaternion.Euler(0f, 0, 0f);

        _curIndexPoint = _curIndexPoint == 0 ? 1 : 0;
    }


    private void OnDrawGizmosSelected()
    {
        //отрисовка движения по точкам
        var point1 = transform.position;
        var point2 = transform.position + Vector3.right * _movementRange;

        if (Application.isPlaying)
        {
            point1 = _waypoints[0];
            point2 = _waypoints[1];
        }

        point1.y = transform.position.y;
        point2.y = transform.position.y;

        Gizmos.DrawSphere(point1, 0.2f);
        Gizmos.DrawSphere(point2, 0.2f);
        Gizmos.DrawLine(point1, point2);
    }
}
