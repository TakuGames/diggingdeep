﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//bug if you fall on bat
public class Bat : AEnemy
{
    [SerializeField] private float _mass = 20;
    [SerializeField] private float _speed;
    [SerializeField] private float _changeTargetDelay;

    private Vector3 _velocity;
    private Vector3 _target;
    private Rigidbody2D _rigidbody;
    private Collider2D _territory;
    private float _curTime = 0 ;
    //private GameObject _player;

    private bool _chase = false;


    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _territory = transform.parent.GetComponent<Collider2D>();
        _target = transform.position;
        _curTime = 0;
        //transform.parent.GetComponent<DangerBatTerritory>().InvasionEvent.AddListener(ToPlayer);
        //_player = GameObject.FindWithTag("Player");
    }

    private void Update()
    {
        //if (_chase)
        //    _target = _player.transform.position;
        if ( _territory == null)
            return;
        
        Move();
    }

    private void Move()
    {
        Wander();
        //we check reached the target
        if (Vector3.Distance(transform.position, _target) < 0.5)
        {
            Vector2 boundsSize = _territory.bounds.size / 2;
            Vector3 point = new Vector2(Random.value * (Random.value > 0.5 ? 1 : -1) * boundsSize.x, Random.value * (Random.value > 0.5 ? 1 : -1) * boundsSize.y);
            _target = _territory.bounds.center - point;
        }

    }



    public void Wander()
    {
        //find force
        Vector3 steering = WanderForce(_target, transform.position, _velocity);//useRoute ? this.WanderForce(target, present.position, velocity) : this.WanderCircleForce(velocity);
        //devision by the number of vectors in the path
        steering /= _mass;
        //gaining velocity and format in max step
        _velocity = (_velocity + steering).normalized * _speed * Time.deltaTime;
        //change position
        transform.position += _velocity;

        //transform.up = _velocity;

    }

    private Vector3 WanderForce(Vector3 target, Vector3 presentPosition, Vector3 velocity)
    {
        Vector3 desiderVelocity = target - presentPosition;
        return desiderVelocity - velocity;
    }

    public void ToPlayer()
    {
        _chase = true;
    }

    protected override void MakePlayerDamage(Collision2D collision)
    {
        if (collision.collider.bounds.min.y > _collider.bounds.max.y) return;

        base.MakePlayerDamage(collision);
    }

}
