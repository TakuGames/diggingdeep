﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : AEnemy
{
    [SerializeField] private float _distance = 0.1f;
    [SerializeField] private LayerMask _mask = 0;
    [SerializeField] private float _speed;
    

    private void Update()
    {
        Move();
    }

    public void Move()
    {
        DetectDirection();
        transform.Translate(new Vector3(_speed, 0f, 0f) * Time.deltaTime);
    }

    private void DetectDirection()
    {
        var point = new Vector2(transform.rotation.y == 0 ? _collider.bounds.max.x : _collider.bounds.min.x, transform.position.y);
        RaycastHit2D hit = Physics2D.Raycast(point, transform.right, _distance, _mask);

        if (!hit)
            return;

        //Debug.DrawLine(point, hit.point *10, Color.red, 2f);

        if (hit.collider)
        {
            ChangeDirection();
        }
    }

    private void ChangeDirection()
    {
        if (transform.rotation.y == 0)
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        else
            transform.rotation = Quaternion.Euler(0f, 0, 0f);
    }

    protected override void MakePlayerDamage(Collision2D collision)
    {
        if (collision.collider.bounds.min.y > _collider.bounds.max.y) return;

        base.MakePlayerDamage(collision);
    }
}
