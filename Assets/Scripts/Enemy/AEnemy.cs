﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AEnemy : MonoBehaviour ,IDamageble
{
    
    [SerializeField] private float _damageAmount;

    protected Collider2D _collider;

    void Awake()
    {
        _collider = GetComponent<Collider2D>();
    }

    public void Damage(float damageCount)
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.collider.name);
        MakePlayerDamage(collision);

    }

    protected virtual void MakePlayerDamage(Collision2D collision)
    {
        if (collision.collider.tag != "Player") return;

        var damagedObject = collision.collider.GetComponent<IDamageble>();

        if (damagedObject != null)
        {
            damagedObject.Damage(_damageAmount);
            Debug.Log("OK");
        }
    }
}
