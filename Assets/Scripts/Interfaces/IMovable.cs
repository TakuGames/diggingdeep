﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable
{
    void Move(PlayerController playerController);
    void Rebound(PlayerController playerController);
}

