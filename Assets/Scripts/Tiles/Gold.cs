﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : MonoBehaviour
{
    [SerializeField] private int _count = 0;

    public void TakeGold()
    {
        ScoreController.GoldValue += _count;
    }
}
