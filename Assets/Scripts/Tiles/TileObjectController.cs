﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TileObjectController : MonoBehaviour
{
    private Tilemap _tileMap;
    // Start is called before the first frame update
    void Start()
    {
        _tileMap = GetComponent<Tilemap>();

        foreach (Vector3Int position in _tileMap.cellBounds.allPositionsWithin)
        {
            var t = _tileMap.GetTile<RuleTile>(position); //_tileMap.GetTile<AScriptableObjectTile>(position);
            if (t != null)
            {
                GameObject Prefab;
                if (t.GetRuleObject(position) != null)
                {
                    Prefab = t.GetRuleObject(position);
                }
                else {
                    if (t.Prefab == null)
                        continue;
                    else
                        Prefab = t.Prefab;
                }
                if(t.DeleteTile)
                    _tileMap.SetTile(position, null);

                Vector3 positionGO = _tileMap.transform.position + position;                
                var go = Instantiate(Prefab, positionGO + new Vector3(0.5f,0.5f,0), Quaternion.identity,transform);                
                go.GetComponent<TilePosition>().Position = position;
            }
        }


    }

    public void AddTileObject(Vector3Int position, GameObject tilePefab, Tile tile)
    {
        _tileMap.SetTile(position, tile);
        Vector3 positionGO = _tileMap.transform.position + position + new Vector3(0.5f, 0.5f, 0);
        var go = Instantiate(tilePefab, positionGO, Quaternion.identity, gameObject.transform);
        go.GetComponent<TilePosition>().Position = position;
    }

    public Tile GetTile(Vector3Int position)
    {
        return (Tile)_tileMap.GetTile(position);
    }
}
