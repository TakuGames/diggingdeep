﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Gravel : ADistructableTile, IDamageble
{
    [SerializeField] private AnimationCurve _fallCurve;
    [SerializeField] private float _duration;
    private Collider2D _collider;
    private float _distance;
    private bool _isMoving = false;
    //private Vector2 target;


    private void Start()
    {
        _collider = GetComponent<Collider2D>();

        StartCoroutine(CheckDownWay());

        _distance = _collider.bounds.size.y * 0.5f + 0.1f;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!_isMoving) return;


        if (collision.collider.tag == "Player" && JoysticDirections.PlayerDirection == EDirection.Up) return;
       

        if (collision.collider.bounds.center.y > _collider.bounds.center.y) return;

        var damagedObject = collision.collider.gameObject.GetComponent<IDamageble>();
        if (damagedObject != null)
            damagedObject.Damage(float.MaxValue);
    }

    public bool Ray()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down,_distance);
        Debug.DrawLine(transform.position, transform.position + Vector3.down * _distance,Color.red,1f);
        if (hit) return false;

        //if (Vector2.Distance(transform.position, hit.point) < _distanceToNextBlock) return false;

        return true;
    }

    private void StartFall()
    {
        _isMoving = true;
        Debug.Log("111");
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOMoveY(transform.position.y - _collider.bounds.size.y, _duration).SetEase(_fallCurve));
        seq.AppendCallback(IsGround);
        seq.Play();

        //if (isMove)
        //{
        //    seq.Append(transform.DOMoveY(target, _duration);
        //}
        //else
        //{
        //    seq.Append(transform.DOMoveY(target, _duration).SetEase(_fallCurve));
        //}

        //    seq.AppendCallback(StartRay);
        //    seq.Play();
    }

    private void IsGround()
    {
        if (!Ray())
        {
            _isMoving = false;
            StartCoroutine(CheckDownWay());
            return;
        }

        Debug.Log("222");
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOMoveY(transform.position.y - _collider.bounds.size.y, _duration).SetEase(Ease.Linear));
        seq.AppendCallback(IsGround);
        seq.Play();
    }

    private IEnumerator CheckDownWay()
    {
        var waitFrame = new WaitForEndOfFrame();
        var waitStartFall = new WaitForSeconds(1f);
        while (true)
        {
            yield return waitFrame;
            if (Ray())
            {
                yield return waitStartFall;
                StartFall();
                yield break;
            }
        }
    }



    public void Damage(float damageCount)
    {
        base.DestroyTile(); 
    }
}
