﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class LookDirectionRaycastEvent : UnityEvent<EDirection, GameObject, float> { }

public class LookInDirection : MonoBehaviour, IRayable
{
    [SerializeField] private float _lenghtOfRay = 1;
    [SerializeField] private float _lenghForcedRay = 1;

    [HideInInspector] public LookDirectionRaycastEvent LookRayEvent = new LookDirectionRaycastEvent();

    private Vector2 _rayPoint;
    private Vector2 _direct;

    public float LenghForcedRay => _lenghForcedRay;

    public void Looking(PlayerController controller)
    {
        #region OLD
        // _rayPoint = new Vector2(controller.transform.position.x, controller.Collider.bounds.min.y);

        //RaycastHit2D hitDown = Physics2D.Raycast(_rayPoint, Vector2.down, LenghForcedRay);

        //if (hitDown.collider)
        //{
        //    float lenght = (_rayPoint - hitDown.point).magnitude;

        //    LookEvent.Invoke(EDirection.Down, hitDown.collider.gameObject, lenght);
        //    Debug.DrawLine(_rayPoint, hitDown.point, Color.red);
        //    return;
        //}
        #endregion

        switch (JoysticDirections.PlayerDirection)
        {
            case EDirection.None:
                #region OLD
                //_direct = Vector2.down;
                //_rayPoint = new Vector2(controller.transform.position.x, controller.Collider.bounds.min.y);
                //break;
                #endregion
                return;
            case EDirection.Right:
                _direct = Vector2.right;
                _rayPoint = new Vector2(controller.Collider.bounds.max.x, controller.transform.position.y);
                break;
            case EDirection.Left:
                _direct = Vector2.left;
                _rayPoint = new Vector2(controller.Collider.bounds.min.x, controller.transform.position.y);
                break;
            case EDirection.Down:
                _direct = Vector2.down;
                _rayPoint = new Vector2(controller.transform.position.x, controller.Collider.bounds.min.y);
                break;
            case EDirection.Up:
                _direct = Vector2.up;
                _rayPoint = new Vector2(controller.transform.position.x, controller.Collider.bounds.max.y);
                break;
            default:
                break;
        }



        RaycastHit2D hit = Physics2D.CircleCast(controller.transform.position, controller.Collider.bounds.size.x * 0.5f , _direct, _lenghtOfRay); //Physics2D.Raycast(controller.transform.position, _direct, _lenghtOfRay);

        #region Draw Debug
        Vector2 drawPoint;
        drawPoint.x = controller.Collider.bounds.min.x;
        drawPoint.y = controller.Collider.bounds.min.y;
        Debug.DrawLine(drawPoint, drawPoint + _direct * _lenghtOfRay, Color.blue, 2f);
        drawPoint.y = controller.Collider.bounds.max.y;
        Debug.DrawLine(drawPoint, drawPoint + _direct * _lenghtOfRay, Color.blue, 2f);
        #endregion

        if (hit.collider)
            LookRayEvent.Invoke(JoysticDirections.PlayerDirection, hit.collider.gameObject, (_rayPoint - hit.point).magnitude);
    }
}
