﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBorders : MonoBehaviour
{
    [SerializeField] private PolygonCollider2D _collider;

    private void Start()
    {
        Vector2[] points = new Vector2[4];
        
        points[0].x = (LevelPlacer.width - 1f) * 0.5f;
        points[0].y = LevelPlacer.top;
        
        points[1].x = (LevelPlacer.width - 1f) * -0.5f;
        points[1].y = LevelPlacer.top;
        
        points[2].x = (LevelPlacer.width - 1f) * -0.5f;
        points[2].y = LevelPlacer.bottom;
        
        points[3].x = (LevelPlacer.width - 1f) * 0.5f;
        points[3].y = LevelPlacer.bottom;

        _collider.points = points;
    }
}
