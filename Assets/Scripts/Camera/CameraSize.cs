﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraSize : MonoBehaviour
{
    private void Start()
    {
        GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize = (LevelPlacer.width - 1) / (Camera.main.aspect * 2);
    }
}
