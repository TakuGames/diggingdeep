﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

//[Serializable]
//public class DiggingEvent : UnityEvent { }

public class Shovel : MonoBehaviour
{
    [SerializeField] private float _destroyDistance = 0.1f;
    [SerializeField] private float _drillingDistanceAnimation = 0.5f;

    [SerializeField] private float _distanceReturnToDown = 0.5f;


    [SerializeField] private float _jumpAnimDistance = 0.1f;
    [SerializeField] private float _reloadDelay = 0.1f;

    [Header("Overlap")]
    [SerializeField] private float _radius = 0.3f;

    //public DiggingEvent DiggingEvent;

    private LookInDirection _lookInDirection;
    private PlayerController _playerController;
    private Animator _animator;
    private float _curReloadTime = 0;

    private Collider2D _playerCollider;

    private bool _onlyDown = false;
    //private bool _drilling = false;

    private void Start()
    {
        JoysticDirections.ChangeDirectionEvent += ChangeAnimation;
        _playerController = GetComponent<PlayerController>();
        _animator = GetComponent<Animator>();
        _playerCollider = GetComponent<Collider2D>();
        _lookInDirection = GetComponent<LookInDirection>();
        _lookInDirection.LookRayEvent.AddListener(ShovelDamaging);

        if (_lookInDirection.LenghForcedRay < _distanceReturnToDown)
            throw new ArgumentException("_distanceReturnToDown should be less than LookDirection.LenghForcedRay");

        _curReloadTime = 0;
    }


    private void Update()
    {
        _curReloadTime += Time.deltaTime;
        //CreateOverlap();
    }


    private void ChangeAnimation(EDirection oldDirection, EDirection newDirection)
    {
        //TODO Make to one line
        switch (oldDirection)
        {
            case EDirection.Down:
                _animator.SetBool("Down", false);
                break;
            case EDirection.Left:
                _animator.SetBool("Left", false);
                break;
            case EDirection.Right:
                _animator.SetBool("Right", false);
                break;
            case EDirection.Up:
                _animator.SetBool("Up", false);
                break;
        }

        switch (newDirection)
        {
            case EDirection.Left:
                _animator.SetBool("Left", true);
                break;
            case EDirection.Right:
                _animator.SetBool("Right", true);
                break;
            case EDirection.Up:
                _animator.SetBool("Up", true);
                break;
            case EDirection.None:
                break;
            default:
                _animator.SetBool("Down", true);
                break;
        }
       
    }

    public void ShovelDamaging(EDirection direction, GameObject targetObject, float distance)
    {
        

        if (distance < _drillingDistanceAnimation)
            _animator.SetTrigger("Drill");

        if (_curReloadTime < _reloadDelay) return;

        #region Exception
        if (_drillingDistanceAnimation <= _destroyDistance)
            throw new ArgumentException("DrillingDistance should be more or equels DestroyDistance");
        #endregion

        if (distance > _drillingDistanceAnimation) return;


        _animator.SetTrigger("Drill");


        MakeDamage(targetObject);
        

        if (targetObject.tag != "Enemy")
            _curReloadTime = 0;
    }

    private void MakeDamage(GameObject enemy)
    {
        var damagedObject = enemy.GetComponent<IDamageble>();

        if (damagedObject != null)
        {
            damagedObject.Damage(_playerController.Config.DamageAmount);
        }
    }

    private void CreateOverlap()
    {
        Vector2 overlapPoint;
        switch (JoysticDirections.PlayerDirection)
        {
            case EDirection.Left:
                overlapPoint = new Vector2(_playerCollider.bounds.min.x, _playerCollider.bounds.center.y);
                break;
            case EDirection.Right:
                overlapPoint = new Vector2(_playerCollider.bounds.max.x, _playerCollider.bounds.center.y);
                break;
            case EDirection.Up:
                overlapPoint = new Vector2(_playerCollider.bounds.center.x, _playerCollider.bounds.max.y);
                break;
            case EDirection.Down:
                overlapPoint = new Vector2(_playerCollider.bounds.center.x, _playerCollider.bounds.min.y);
                break;
            default:
                return;
        }

        var itemInOverlap = Physics2D.OverlapCircleAll(overlapPoint, _radius);
        DebugExtension.DebugCircle(overlapPoint, new Vector3(0, 0, 1), Color.red, _radius);

        foreach (var item in itemInOverlap)
        {
            if (item.CompareTag("Enemy"))
            {
                MakeDamage(item.gameObject);
            }
        }
    }

    private void OnDestroy()
    {
        JoysticDirections.ChangeDirectionEvent -= ChangeAnimation;
        _lookInDirection.LookRayEvent.RemoveListener(ShovelDamaging);
    }
}